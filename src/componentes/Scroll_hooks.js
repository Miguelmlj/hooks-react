import React, {useState, useEffect} from 'react';

export default function ScrollHooks (){

    const [scrollY, setScrollY] = useState(0);
    const [name, setName] = useState("Jon");

    // useEffect(() => {
    //     console.log('fase de montaje');

    //     const detectarScroll = () => {
    //         setScrollY(window.pageYOffset);
    //     }

    //     window.addEventListener("scroll", detectarScroll);        

    // });


    useEffect(() => {
        console.log('moviendo el scroll'); //lo que hay en un use effect, se vuelve a actualizar(renderizar) cada vez que se vuelva a repintar el componente

        const detectarScroll = () => {
            setScrollY(window.pageYOffset);
        }

        window.addEventListener("scroll", detectarScroll);     
        
        return () => {
            window.removeEventListener("scroll", detectarScroll);
            console.log('fase de desmontaje');
        }

    }, [scrollY]);



    useEffect(() => {
        console.log('fase de montaje'); //si quieres fase de montaje: deja el array vacio
        //este useefect solo se va a ejecutar cuando la variable scrollY cambie !! <para que se ejecute este useefect solamente una vez, se debe de dejar el segundo argumento vacio [] corchetes vacíos>
       
    }, []); //[scrollY]



    useEffect(() => {
        console.log('fase de actualización');
        //un useefect que no tiene el segundo parámetro que es la lista de dependenciaas, va a ser como un component DidUpdate: cada vez que se vuelva a renderizar el componente, se va a ejecutar todo lo que este en este efecto
       
    });

    useEffect(() => {
        //la fase de desmontaje del efecto se realiza cuando llamamos un return function: aquí puede ser una buen aoportunidad para desconectarte de servicios, desconectarte de APiS, limpiar intervalos de tiempo, limpiar manejadores de eventos

       return () => {
           console.log('fase de desmontaje');
       }
       
    });



    return(
        <>
        <h2>Hooks = useEffect y el Ciclo de Vida - {scrollY}</h2>

        </>
    )

}