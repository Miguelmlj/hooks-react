import logo from './logo.svg';
import './App.css';
import ContadorHooks from './componentes/Contador_hooks';
import ScrollHooks from './componentes/Scroll_hooks';
import RelojHooks from './componentes/RelojHooks';

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <section>
        <ContadorHooks titulo="Seguidores"/>

        <hr/>
        <ScrollHooks/>
        <hr/>
        <RelojHooks/>
        

       </section>
      </header>
    </div>
  );
}

export default App;
